#   Ork

Ork is a general-purpose command-line utility for ops and devs (mostly ops) that has the following design goals:

1. Provide a unified interface for all our ops needs, wrapping other tools like ansible, terraform, aws-cli, etc.
2. Provide a sensible API to reduce friction, so underlying mechanics can change often without breaking the interface.
3. Don't worry, be happy.

![ork](ork.jpg)
